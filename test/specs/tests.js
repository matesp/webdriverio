import HomePage from  '../pageobjects/home.page';
import SearchPage from '../pageobjects/search.page';
import ItemPage from '../pageobjects/item.page';

const stringToSearch = "pintura blanca"

describe('Item search and price comparison', () => {
    it('should search item "pintura blanca" and verify that the prices of PLP and PDP are the same', async () => {
        //maximize the window for a correct execution
        browser.maximizeWindow()
        await HomePage.open();
        await HomePage.search(stringToSearch);
        //verify that the price is displayed in PLP
        await expectAsync(SearchPage.firstItemPrice).toBeExisting();
        await SearchPage.openItemPage();
        //compare both prices
        expectAsync(SearchPage.firstItemPrice.isEqual(ItemPage.itemPrice))
    });
});