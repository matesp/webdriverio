import Page from './page';

class HomePage extends Page {
    get inputSearchBar () { return $('//*[@id="header-med-search-bar-SearchBar-79247401-c89a-4b16-9d3c-f91206c21c2c-desktop"]/input') }

    async search (wordToSearch) {
        //set the value to search and then hit enter key
        await this.inputSearchBar.setValue(wordToSearch);
        browser.keys("\uE007"); 
    }

    open () {
        return super.open('sodimac-cl');
    }
}

export default new HomePage();