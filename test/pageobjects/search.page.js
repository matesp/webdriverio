import Page from './page';

class SearchPage extends Page {
    get firstItemBtn () { return $('(//div[@class="jsx-411745769 product-image"])[1]') }
    get firstItemPrice () { return $('(//div[@class="jsx-411745769 desktop-price-cart-btn"]//div[@class="jsx-4135487716 price jsx-175035124"]//span)[1]') }

    //open the first PDP
    async openItemPage () {
        await this.firstItemBtn.click();
    }
}

export default new SearchPage();