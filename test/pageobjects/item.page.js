import Page from './page';

class ItemPage extends Page {
    get itemPrice () { return $('//*[@id="__next"]/div/div/div[4]/div[2]/div[1]/div[2]/div[2]/div/div/div/span[1]') }
}

export default new ItemPage();